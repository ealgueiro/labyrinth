package com.resolvit;

import java.util.LinkedList;

public class BreadthFirstSearchStrategy {
	public static void solve(Maze maze) {
		LinkedList<MazePosition> candidates = new LinkedList<MazePosition>();

		// insert start position
		candidates.add(maze.getStartPosition());

		MazePosition currentPosition;
		MazePosition nextPosition;
		while (!candidates.isEmpty()) {
			currentPosition = candidates.pop();

			if (maze.isMazeSolved(currentPosition))
				break;

			// mark the current position
			maze.markPosition(currentPosition, maze.getPathMark());

			maze.printMaze();

			// Check for possible ways to go
	        nextPosition = maze.getPosition(currentPosition, MazePosition.NORTH);
	        if (maze.canMove(nextPosition)) candidates.add(nextPosition);
	        
	        nextPosition = maze.getPosition(currentPosition, MazePosition.EAST);
	        if (maze.canMove(nextPosition)) candidates.add(nextPosition);
	        
	        nextPosition = maze.getPosition(currentPosition, MazePosition.SOUTH);
	        if (maze.canMove(nextPosition)) candidates.add(nextPosition);
	        
	        nextPosition = maze.getPosition(currentPosition, MazePosition.WEST);
	        if (maze.canMove(nextPosition)) candidates.add(nextPosition);			
		}

		System.out.println(!candidates.isEmpty() ? "Done" : "Sorry, could not do it");
		maze.printMaze();
	}
}
