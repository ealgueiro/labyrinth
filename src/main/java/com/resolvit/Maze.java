package com.resolvit;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Maze {

	private char[][] maze;
	private char pathMark = '.';
	private char wall = '_';
	private char emptySpace = ' ';
	private char entrance = 'I';
	private char exit = 'E';
	private MazePosition startPosition;
	private MazePosition exitPosition;

	public boolean isMazeSolved(MazePosition currentPosition) {
		return currentPosition.getY() == exitPosition.getY() && currentPosition.getX() == exitPosition.getX();
	}

	public void markPosition(MazePosition currentPosition, char pathMark) {
		maze[currentPosition.getY()][currentPosition.getX()] = pathMark;
	}

	public boolean canMove(MazePosition nextPosition) {
		boolean result = false;
		if (nextPosition != null) {
			char value = maze[nextPosition.getY()][nextPosition.getX()];
			if (value == this.emptySpace || value == this.exit) {
				result = true;
			}
		}
		return result;
	}

	public void printMaze() {
		for (int i = 0; i < maze.length; i++) {
			for (int j = 0; j < maze[i].length; j++) {
				System.out.print(maze[i][j]);
			}
			System.out.print("\r\n");
		}
	}

	public MazePosition getPosition(MazePosition currentPosition, String direction) {
		MazePosition position = null;

		int currentPositionX = currentPosition.getX();
		int currentPositionY = currentPosition.getY();

		/**
		 * 0123X
		 * 1
		 * 2
		 * 3
		 * Y
		 */
		if (direction.equals(MazePosition.NORTH))
			currentPositionY--;
		if (direction.equals(MazePosition.SOUTH))
			currentPositionY++;
		if (direction.equals(MazePosition.WEST))
			currentPositionX--;
		if (direction.equals(MazePosition.EAST))
			currentPositionX++;

		try {
			char value = maze[currentPositionY][currentPositionX];
			position = new MazePosition(currentPositionX, currentPositionY);
		} catch (Exception e) {
			// out of bounds
		}

		return position;
	}

	public void readMazeFromFile(String fileName) {

		try {
			FileReader fileReader = new FileReader(fileName);
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			List<char[]> list = new ArrayList<char[]>();

			String line = bufferedReader.readLine();
			int lineSize = 0;
			while (line != null) {
				char[] lineCharArray = line.toCharArray();
				lineSize = lineCharArray.length;
				list.add(lineCharArray);
				line = bufferedReader.readLine();
			}

			char[][] mazeNew = new char[list.size()][lineSize];

			int i = 0;
			for (char[] lineCharArray : list) {
				for (int j = 0; j < lineSize; j++) {
					mazeNew[i][j] = lineCharArray[j];
					if (mazeNew[i][j] == this.getEntrance())
						this.startPosition = new MazePosition(j, i);
					if (mazeNew[i][j] == this.getExit())
						this.exitPosition = new MazePosition(j, i);
				}
				i++;
			}

			this.maze = mazeNew;

			fileReader.close();
			bufferedReader.close();

		} catch (FileNotFoundException ex) {
			System.out.println("Unable to open file: " + fileName);
		} catch (IOException ex) {
			System.out.println("Error reading file: " + fileName);
		}

	}

	public char[][] getMaze() {
		return maze;
	}

	public void setMaze(char[][] maze) {
		this.maze = maze;
	}

	public void setPathMark(char pathMark) {
		this.pathMark = pathMark;
	}

	public char getPathMark() {
		return this.pathMark;
	}

	public char getWall() {
		return wall;
	}

	public void setWall(char wall) {
		this.wall = wall;
	}

	public char getEmptySpace() {
		return emptySpace;
	}

	public void setEmptySpace(char emptySpace) {
		this.emptySpace = emptySpace;
	}

	public char getEntrance() {
		return entrance;
	}

	public void setEntrance(char entrance) {
		this.entrance = entrance;
	}

	public char getExit() {
		return exit;
	}

	public void setExit(char exit) {
		this.exit = exit;
	}

	public MazePosition getStartPosition() {
		return startPosition;
	}

	public void setStartPosition(MazePosition startPosition) {
		this.startPosition = startPosition;
	}

	public MazePosition getExitPosition() {
		return exitPosition;
	}

	public void setExitPosition(MazePosition exitPosition) {
		this.exitPosition = exitPosition;
	}

}
