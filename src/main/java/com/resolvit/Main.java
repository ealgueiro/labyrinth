package com.resolvit;

public class Main {

	public static void main(String[] args) {
		if (args == null || args.length == 0) {
			System.out.println("Error: please provide a valid path for the Maze file. e.g. maze1.txt");
			return;
		}

		String fileName = args[0];

		Maze maze = new Maze();
		maze.readMazeFromFile(fileName);
		//maze.printMaze();
		BreadthFirstSearchStrategy.solve(maze);
	}

}
